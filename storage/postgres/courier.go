package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/delivery_microservices/delivery_go_user_service/genproto/user_service"
)

type CourierRepo struct {
	db *pgxpool.Pool
}

func NewCourierRepo(db *pgxpool.Pool) *CourierRepo {
	return &CourierRepo{
		db: db,
	}
}

func (r *CourierRepo) Create(ctx context.Context, req *user_service.CourierCreateReq) (*user_service.CourierCreateResp, error) {
	var id int
	query := `
	INSERT INTO couriers (
		first_name,
		last_name,
		phone,
		login,
		password,
		max_order_count,
		branch_id
	)
	VALUES ($1,$2,$3,$4,$5,$6,$7)
	RETURNING id;`

	if err := r.db.QueryRow(ctx, query,
		req.FirstName,
		req.LastName,
		req.Phone,
		req.Login,
		req.Password,
		req.MaxOrderCount,
		req.BranchId,
	).Scan(&id); err != nil {
		return nil, err
	}

	return &user_service.CourierCreateResp{
		Msg: fmt.Sprintf("%d", id),
	}, nil
}

func (r *CourierRepo) GetByUsername(ctx context.Context, req *user_service.CourierGetByUsernameReq) (*user_service.Courier, error) {
	query := `
	SELECT
		id,
		first_name,
		last_name,
		phone,
		active,
		login,
		password,
		max_order_count,
		branch_id,
		created_at::TEXT,
		updated_at::TEXT 
	FROM couriers 
	WHERE login = $1;`

	var courier = user_service.Courier{}
	if err := r.db.QueryRow(ctx, query, req.Username).Scan(
		&courier.Id,
		&courier.FirstName,
		&courier.LastName,
		&courier.Phone,
		&courier.Active,
		&courier.Login,
		&courier.Password,
		&courier.MaxOrderCount,
		&courier.BranchId,
		&courier.CreatedAt,
		&courier.UpdatedAt,
	); err != nil {
		return nil, err
	}

	return &courier, nil
}
