package postgres

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/delivery_microservices/delivery_go_user_service/config"
	"gitlab.com/delivery_microservices/delivery_go_user_service/storage"
)

type Store struct {
	db       *pgxpool.Pool
	branches *BranchRepo
	users    *UserRepo
	couriers *CourierRepo
	clients  *ClientRepo
}

func NewPostgres(ctx context.Context, cfg config.Config) (storage.StorageI, error) {
	config, err := pgxpool.ParseConfig(
		fmt.Sprintf("postgres://%s:%s@%s:%d/%s?sslmode=disable",
			cfg.PostgresUser,
			cfg.PostgresPassword,
			cfg.PostgresHost,
			cfg.PostgresPort,
			cfg.PostgresDatabase,
		),
	)
	if err != nil {
		return nil, err
	}

	config.MaxConns = cfg.PostgresMaxConnections
	pool, err := pgxpool.NewWithConfig(ctx, config)
	if err != nil {
		return nil, err
	}

	return &Store{
		db: pool,
	}, nil
}

func (s *Store) CloseDB() {
	s.db.Close()
}

func (s *Store) Branch() storage.BranchRepoI {
	if s.branches == nil {
		s.branches = NewBranchRepo(s.db)
	}
	return s.branches
}

func (s *Store) User() storage.UserRepoI {
	if s.users == nil {
		s.users = NewUserRepo(s.db)
	}

	return s.users
}

func (s *Store) Courier() storage.CourierRepoI {
	if s.couriers == nil {
		s.couriers = NewCourierRepo(s.db)
	}

	return s.couriers
}

func (s *Store) Client() storage.ClientRepoI {
	if s.clients == nil {
		s.clients = NewClientRepo(s.db)
	}

	return s.clients
}
