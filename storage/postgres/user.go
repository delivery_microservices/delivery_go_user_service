package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"

	"gitlab.com/delivery_microservices/delivery_go_user_service/genproto/user_service"
)

type UserRepo struct {
	db *pgxpool.Pool
}

func NewUserRepo(db *pgxpool.Pool) *UserRepo {
	return &UserRepo{
		db: db,
	}
}

func (r *UserRepo) Create(ctx context.Context, req *user_service.UserCreateReq) (*user_service.UserCreateResp, error) {
	var id int
	query := `
	INSERT INTO users (
		first_name,
		last_name,
		branch_id,
		phone,
		login,
		password
	)
	VALUES ($1,$2,$3,$4,$5,$6)
	RETURNING id;`

	if err := r.db.QueryRow(ctx, query,
		req.FirstName,
		req.LastName,
		req.BranchId,
		req.Phone,
		req.Login,
		req.Password,
	).Scan(&id); err != nil {
		return nil, err
	}

	return &user_service.UserCreateResp{
		Msg: fmt.Sprintf("%d", id),
	}, nil
}

func (r *UserRepo) GetByUsername(ctx context.Context, req *user_service.UserGetByUsernameReq) (*user_service.User, error) {
	var (
		updatedAt sql.NullString
	)
	query := `
	SELECT
		id,
		first_name,
		last_name,
		branch_id,
		phone,
		active,
		login,
		password,
		created_at::TEXT,
		updated_at::TEXT 
	FROM users 
	WHERE login = $1 AND active='yes';`

	var user = user_service.User{}
	if err := r.db.QueryRow(ctx, query, req.Username).Scan(
		&user.Id,
		&user.FirstName,
		&user.LastName,
		&user.BranchId,
		&user.Phone,
		&user.Active,
		&user.Login,
		&user.Password,
		&user.CreatedAt,
		&updatedAt,
	); err != nil {
		return nil, err
	}

	user.UpdatedAt = updatedAt.String

	return &user, nil
}
