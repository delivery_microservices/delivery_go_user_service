package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/delivery_microservices/delivery_go_user_service/genproto/user_service"
)

type BranchRepo struct {
	db *pgxpool.Pool
}

func NewBranchRepo(db *pgxpool.Pool) *BranchRepo {
	return &BranchRepo{
		db: db,
	}
}

func (r *BranchRepo) Create(ctx context.Context, req *user_service.BranchCreateReq) (*user_service.BranchCreateResp, error) {
	var id int
	query := `
	INSERT INTO branches (
		name,
		phone,
		photo,
		delivery_tarif_id,
		work_hour_start,
		work_hour_end,
		address,
		destination
	)
	VALUES ($1,$2,$3,$4,$5,$6,$7,$8)
	RETURNING id;`

	if err := r.db.QueryRow(ctx, query,
		req.Name,
		req.Phone,
		req.Photo,
		req.DeliveryTarifId,
		req.WorkHourStart,
		req.WorkHourEnd,
		req.Address,
		req.Destination,
	).Scan(&id); err != nil {
		return nil, err
	}

	return &user_service.BranchCreateResp{
		Msg: fmt.Sprintf("%d", id),
	}, nil
}

func (r *BranchRepo) GetList(ctx context.Context, req *user_service.BranchGetListReq) (*user_service.BranchGetListResp, error) {
	var (
		filter    = " WHERE deleted_at IS NULL "
		offsetQ   = " OFFSET 0;"
		limit     = " LIMIT 10 "
		offset    = (req.Page - 1) * req.Limit
		count     int
		updatedAt sql.NullString
	)

	s := `
	SELECT 
		id,
		name,
		phone,
		photo,
		delivery_tarif_id,
		work_hour_start::TEXT,
		work_hour_end::TEXT,
		address,
		destination,
		created_at::TEXT,
		updated_at::TEXT 
	FROM branches `

	if req.Name != "" {
		filter += ` AND name ILIKE ` + "'%" + req.Name + "%' "
	}
	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}
	if req.CreatedAtFrom != "" {
		filter += " AND created_at BETWEEN '" + req.CreatedAtFrom + "' AND '" + req.CreatedAtTo + "' "
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM branches` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	resp := &user_service.BranchGetListResp{}
	for rows.Next() {
		var branch = user_service.Branch{}
		if err := rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.Phone,
			&branch.Photo,
			&branch.DeliveryTarifId,
			&branch.WorkHourStart,
			&branch.WorkHourEnd,
			&branch.Address,
			&branch.Destination,
			&branch.CreatedAt,
			&updatedAt,
		); err != nil {
			return nil, err
		}

		branch.UpdatedAt = updatedAt.String

		resp.Branches = append(resp.Branches, &branch)
		resp.Count = int64(count)
	}

	return resp, nil
}

func (r *BranchRepo) GetById(ctx context.Context, req *user_service.BranchIdReq) (*user_service.Branch, error) {
	var (
		updatedAt sql.NullString
	)
	query := `
    SELECT 
		id,
		name,
		phone,
		photo,
		delivery_tarif_id,
		work_hour_start::TEXT,
		work_hour_end::TEXT,
		address,
		destination,
		created_at::TEXT,
		updated_at::TEXT 
    FROM branches 
    WHERE id = $1;`

	var branch = user_service.Branch{}

	if err := r.db.QueryRow(ctx, query, req.Id).Scan(
		&branch.Id,
		&branch.Name,
		&branch.Phone,
		&branch.Photo,
		&branch.DeliveryTarifId,
		&branch.WorkHourStart,
		&branch.WorkHourEnd,
		&branch.Address,
		&branch.Destination,
		&branch.CreatedAt,
		&updatedAt,
	); err != nil {
		return nil, err
	}
	branch.UpdatedAt = updatedAt.String

	return &branch, nil
}

func (r *BranchRepo) Update(ctx context.Context, req *user_service.BranchUpdateReq) (*user_service.BranchUpdateResp, error) {
	query := `
    UPDATE branches 
    SET 
        name = $2,
		phone = $3,
		photo = $4,
		delivery_tarif_id = $5,
		work_hour_start = $6,
		work_hour_end = $7,
        address = $8,
		destination = $9,
        active = $10,
		updated_at = NOW()
    WHERE id = $1;`

	_, err := r.db.Exec(ctx, query,
		req.Id,
		req.Name,
		req.Phone,
		req.Photo,
		req.DeliveryTarifId,
		req.WorkHourStart,
		req.WorkHourEnd,
		req.Address,
		req.Destination,
		req.Active,
	)

	if err != nil {
		return nil, err
	}

	return &user_service.BranchUpdateResp{
		Msg: "success",
	}, nil
}

func (r *BranchRepo) Delete(ctx context.Context, req *user_service.BranchIdReq) (*user_service.BranchDeleteResp, error) {
	query := `
    UPDATE branches 
    SET 
        deleted_at = NOW()
    WHERE id = $1;`

	res, err := r.db.Exec(ctx, query,
		req.Id,
	)

	if err != nil {
		return nil, err
	}

	if res.RowsAffected() == 0 {
		return nil, fmt.Errorf("branch not found")
	}

	return &user_service.BranchDeleteResp{
		Msg: "success",
	}, nil
}

func (r *BranchRepo) GetBranchesActive(ctx context.Context, req *user_service.BranchesActiveGetReq) (*user_service.BranchGetListResp, error) {
	var (
		filter    = " WHERE deleted_at IS NULL AND active='yes' "
		offsetQ   = " OFFSET 0;"
		limit     = " LIMIT 10 "
		offset    = (req.Page - 1) * req.Limit
		count     int
		updatedAt sql.NullString
	)

	s := `
	SELECT 
		id,
		name,
		phone,
		photo,
		delivery_tarif_id,
		work_hour_start::TEXT,
		work_hour_end::TEXT,
		address,
		destination,
		created_at::TEXT,
		updated_at::TEXT 
	FROM branches `

	if req.Limit > 0 {
		limit = fmt.Sprintf("LIMIT %d", req.Limit)
	}
	if offset > 0 {
		offsetQ = fmt.Sprintf("OFFSET %d", offset)
	}
	if req.TimeNow != "" {
		filter += " AND '" + req.TimeNow + "' BETWEEN work_hour_start AND work_hour_end "
	}

	query := s + filter + limit + offsetQ

	countS := `SELECT COUNT(*) FROM branches` + filter

	rows, err := r.db.Query(ctx, query)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	err = r.db.QueryRow(ctx, countS).Scan(&count)
	if err != nil {
		return nil, err
	}

	resp := &user_service.BranchGetListResp{}
	for rows.Next() {
		var branch = user_service.Branch{}
		if err := rows.Scan(
			&branch.Id,
			&branch.Name,
			&branch.Phone,
			&branch.Photo,
			&branch.DeliveryTarifId,
			&branch.WorkHourStart,
			&branch.WorkHourEnd,
			&branch.Address,
			&branch.Destination,
			&branch.CreatedAt,
			&updatedAt,
		); err != nil {
			return nil, err
		}

		branch.UpdatedAt = updatedAt.String

		resp.Branches = append(resp.Branches, &branch)
		resp.Count = int64(count)
	}

	return resp, nil
}
