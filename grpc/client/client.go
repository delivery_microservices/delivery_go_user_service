package client

import "gitlab.com/delivery_microservices/delivery_go_user_service/config"

type ServiceManagerI interface {
}

type grpcClients struct {
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	return &grpcClients{}, nil
}
