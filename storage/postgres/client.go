package postgres

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"
	"gitlab.com/delivery_microservices/delivery_go_user_service/genproto/user_service"
)

type ClientRepo struct {
	db *pgxpool.Pool
}

func NewClientRepo(db *pgxpool.Pool) *ClientRepo {
	return &ClientRepo{
		db: db,
	}
}

func (r *ClientRepo) Create(ctx context.Context, req *user_service.ClientCreateReq) (*user_service.ClientCreateResp, error) {
	var id int
	query := `
	INSERT INTO clients (
		first_name,
		last_name,
		phone,
		photo,
		birth_date,
		discount_type,
		discount_amount
	)
	VALUES ($1,$2,$3,$4,$5,$6,$7)
	RETURNING id;`

	if err := r.db.QueryRow(ctx, query,
		req.FirstName,
		req.LastName,
		req.Phone,
		req.Photo,
		req.BirthDate,
		req.DiscountType,
		req.DiscountAmount,
	).Scan(&id); err != nil {
		return nil, err
	}

	return &user_service.ClientCreateResp{
		Msg: fmt.Sprintf("%d", id),
	}, nil
}

func (r *ClientRepo) GetByUsername(ctx context.Context, req *user_service.ClientGetByUsernameReq) (*user_service.Client, error) {
	var (
		updatedAt sql.NullString
	)
	query := `
	SELECT
		id,
		first_name,
		last_name,
		phone,
		photo,
		birth_date::TEXT,
		last_ordered_date::TEXT,
		total_orders_sum,
		total_orders_count,
		discount_type,
		discount_amount,
		created_at::TEXT,
		updated_at::TEXT 
	FROM couriers 
	WHERE login = $1;`

	var client = user_service.Client{}
	if err := r.db.QueryRow(ctx, query, req.Username).Scan(
		&client.Id,
		&client.FirstName,
		&client.LastName,
		&client.Phone,
		&client.Photo,
		&client.BirthDate,
		&client.LastOrderedDate,
		&client.TotalOrdersSum,
		&client.TotalOrdersCount,
		&client.DiscountType,
		&client.DiscountAmount,
		&client.CreatedAt,
		&updatedAt,
	); err != nil {
		return nil, err
	}

	client.UpdatedAt = updatedAt.String

	return &client, nil
}
