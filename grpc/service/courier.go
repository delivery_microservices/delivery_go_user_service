package service

import (
	"context"

	"gitlab.com/delivery_microservices/delivery_go_user_service/config"
	"gitlab.com/delivery_microservices/delivery_go_user_service/genproto/user_service"
	"gitlab.com/delivery_microservices/delivery_go_user_service/grpc/client"
	"gitlab.com/delivery_microservices/delivery_go_user_service/pkg/logger"
	"gitlab.com/delivery_microservices/delivery_go_user_service/storage"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

type CourierService struct {
	cfg      config.Config
	log      logger.LoggerI
	strg     storage.StorageI
	services client.ServiceManagerI
	*user_service.UnimplementedCourierServiceServer
}

func NewCourierService(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) *CourierService {
	return &CourierService{
		cfg:      cfg,
		log:      log,
		strg:     strg,
		services: srvc,
	}
}

func (u *CourierService) Create(ctx context.Context, req *user_service.CourierCreateReq) (*user_service.CourierCreateResp, error) {
	u.log.Info("====== Courier Create ======", logger.Any("req", req))

	resp, err := u.strg.Courier().Create(ctx, req)
	if err != nil {
		u.log.Error("error while creating courier", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}

func (u *CourierService) GetByUsername(ctx context.Context, req *user_service.CourierGetByUsernameReq) (*user_service.Courier, error) {
	u.log.Info("====== Courier GetByUsername ======", logger.Any("req", req))

	resp, err := u.strg.Courier().GetByUsername(ctx, req)
	if err != nil {
		u.log.Error("error while getting courier", logger.Error(err))
		return nil, status.Error(codes.InvalidArgument, err.Error())
	}

	return resp, nil
}
